# Shiny Demo App

### VPS Setup

Create a docker network:
```bash
$ sudo docker network create shiny-net
```
The CI/CD pipeline builds both the app and the proxy... and then runs the proxy. The only change to a typical VPS docker setup is the necessity to create a docker network called `shiny_net` using the above command.